# dummy-oracle-deb
Generates a dummy java deb to satisfy java dependency when Oracle JVM is installed

## How to build
Execute the build script on Debian/Ubuntu:

  $ ./build.sh
  
## how to install

  $ dpkg -i dummy-oracle-jre_1.8_all.deb
