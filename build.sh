#!/usr/bin/env bash
set -e

# Install equivs package
sudo apt-get install -y equivs

# Set working directory to home
cd ~

# Create control file
cat > dummy-oracle-jre <<EOF
### Commented entries have reasonable defaults.
### Uncomment to edit them.
# Source: <source package name; defaults to package name>
Section: misc
Priority: optional
# Homepage: <enter URL here; no default>
Standards-Version: 3.9.2

Package: dummy-oracle-jdk
Version: 1.8
Provides: java8-runtime-headless,default-jre-headless,java-runtime-headless
Description: dummy package to satisfy java8-runtime-headless dep for manual install
 Use this package if you installed Oracle JDK 1.8 manually
EOF

# Generate deb package
equivs-build dummy-oracle-jre

# Verify results
echo "Dummy package generated:"
ls dummy-oracle-jre_1.8_all.deb
